<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css"
          href="webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <!--
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
    <c:url value="css/main.css" var="jstlCss"/>
    <link href="${jstlCss}" rel="stylesheet"/>
</head>
<body>

<div class="container">
    <form name="propertiesForm">
        <div>
            <label>Key:</label>&nbsp;Section Name. Property Name
            <input type="text" style="width:40%; height:25px" name="key" class="form-control"
                   placeholder="Eg: global.pidfile" required>
            <p></p>
            <label>Value:</label>&nbsp;Property Value
            <input type="text" style="width:40%; height:25px" name="value" class="form-control"
                   placeholder="Eg: /var/dev/data/pid/haproxy.pid" required>
            <br/>
            <button type="submit" class="btn btn-warning save-btn">Add</button>
            <button type="button" class="btn btn-warning save-btn" onclick="location.href='http://localhost:8989';">Back </button>
        </div>

        <br/>

        <div>
            <table class="table table-bordered data-table" id="propTable" >
                <thead>
                <th>Key</th>
                <th>Value</th>
                <th width="450px">Action</th>
                </thead>
                <tbody id="confdata">
                <c:forEach items="${props}" var="element" varStatus="loop">
                    <tr data-key='${element.key}' data-value='${element.value}'>
                        <td class="text1">${element.key}</td>
                        <td class="text2">${element.value}</td>
                        <td>
                            <button class='btn btn-info btn-xs btn-edit'>Edit</button>
                            <button class='btn btn-danger btn-xs btn-delete'>Delete</button>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </form>
    <p></p>
    <div>
        <div>
            <button type="submit" class="btn btn-success save-btn" id="btnYaml">Save As YAML</button> &nbsp;
            <!--
            <button type="submit" class="btn btn-warning warning-btn" id="btnJson">Save As JSON</button> &nbsp; &nbsp;
            <button type="submit" class="btn btn-danger danger-btn" id="btnProp">Save As PROP</button> &nbsp; &nbsp;
            -->
        </div>
        <p></p>
        <p></p>
        <hr>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#btnYaml").click(function (event) {
            event.preventDefault();
            var win = window.open('','Layout Preview',
                                     'location=no,menubar=no,titlebar=yes,status=no,toolbar=yes,resizable=yes,scrollbars=yes,' +
                                     'top=100,left=400,width=500,height=800', true);
            /*
            // This is for sending data as Json uncomment it for use
            var keyValues = [];
            keyValues.push(encodeURIComponent("set1"+"\t"+"http-request set-path1 %[path,lower]"));
            keyValues.push(encodeURIComponent("set2"+"\t"+"http-request set-path2 %[path,lower]"));
            keyValues.push(encodeURIComponent("set3"+"\t"+"http-request set-path2 %[path,lower]"));
             $.ajax({
                //contentType: "application/json",
                url: "/yaml",
                type: "POST",
                //data: JSON.stringify(keyValues),
                 data:  'keyValues='+ encodeURIComponent(keyValues),
                cache: false,
                success: function (data) {
                    win.document.write('<html><style>.m1 {font-family: Verdana, Geneva, Tahoma, sans-serif;font-size: smaller;}</style><body class="m1"><div style="margin-left: 30px">');
                    win.document.writeln(data);
                    win.document.write('</div></body></html>');
                    win.document.close()
                    console.log("SUCCESS : ", data);
                },
                error: function (e) {
                    $('#result').html(e.responseText);
                    console.log("ERROR : ", e);
                }
            });
            */
            var singleRow='';
            var values = $('#confdata tr')  // Find all <td> elements inside of an element with id "test".
                .map(function(i, e){// Transform all found elements to a list of jQuery objects...
                    //singleRow = JSON.stringify(encodeURIComponent(e.innerText.replace('Edit Delete','')));
                    singleRow = e.innerText.replace('Edit Delete','');
                    //return decodeURIComponent(singleRow); // ... using the element's innerText property as the value.
                    return encodeURIComponent(singleRow); // ... using the element's innerText property as the value.
                })
                .get();
            $.ajax({
                url: "/yaml",
                type: "POST",
                data:  'values='+ encodeURIComponent(values),
                cache: false,
                success: function (data) {
                    win.document.write('<html><style>.m1 {font-family: Verdana, Geneva, Tahoma, sans-serif;font-size: smaller;}</style><body class="m1"><div style="margin-left: 30px">');
                    win.document.writeln(data);
                    win.document.write('</div></body></html>');
                    win.document.close()
                    console.log("SUCCESS : ", data);
                },
                error: function (e) {
                    $('#result').html(e.responseText);
                    console.log("ERROR : ", e);
                }
            });
        });
    });


    $("form").submit(function (e) {
        e.preventDefault();
        var key = $("input[name='key']").val();
        var value = $("input[name='value']").val();
        $(".data-table tbody").append(
            "<tr data-key='" + key + "' data-value='" + value + "'>" +
            "<td class='text1'>" + key + "</td>" +
            "<td class='text2'>" + value + "</td>" +
            "<td><button class='btn btn-info btn-xs btn-edit'>Edit</button>" +
            "&nbsp;<button class='btn btn-danger btn-xs btn-delete'>Delete</button></td></tr>");
        $("input[name='key']").val('');
        $("input[name='value']").val('');
    });
    $("body").on("click", ".btn-delete", function () {
        var txt;
        if (confirm("Do you want to continue to delete?")) {
            txt = "ok";
        } else {
            txt = "cancel";
        }
        if (txt == 'ok') {
            $(this).parents("tr").remove();
        }
    });

    $("body").on("click", ".btn-edit", function (event) {
        event.preventDefault();
        //var key = $(this).parents("tr").attr('data-key');
        var value = $(this).parents("tr").attr('data-value');
        // $(this).parents("tr").find("td:eq(0)").html('<input name="edit_key" value="'+key+'">');
        $(this).parents("tr").find("td:eq(1)").html('<input name="edit_value" value="' + value + '">');
        $(this).parents("tr").find("td:eq(2)").prepend(
            "<button class='btn btn-info btn-xs btn-update'>Update</button>" +
            "<button class='btn btn-warning btn-xs btn-cancel'>Cancel</button>")
        $(this).hide();
    });

    $("body").on("click", ".btn-cancel", function () {
        var key = $(this).parents("tr").attr('data-key');
        var value = $(this).parents("tr").attr('data-value');
        $(this).parents("tr").find("td:eq(0)").text(key);
        $(this).parents("tr").find("td:eq(1)").text(value);
        $(this).parents("tr").find(".btn-edit").show();
        $(this).parents("tr").find(".btn-update").remove();
        $(this).parents("tr").find(".btn-cancel").remove();
    });

    $("body").on("click", ".btn-update", function () {

        var key = $(this).parents("tr").find("input[name='edit_key']").val();
        var value = $(this).parents("tr").find("input[name='edit_value']").val();

        $(this).parents("tr").find("td:eq(0)").text(key);
        $(this).parents("tr").find("td:eq(1)").text(value);

        $(this).parents("tr").attr('data-key', key);
        $(this).parents("tr").attr('data-value', value);

        $(this).parents("tr").find(".btn-edit").show();
        $(this).parents("tr").find(".btn-cancel").remove();
        $(this).parents("tr").find(".btn-update").remove();
    });

</script>

</body>
</html>