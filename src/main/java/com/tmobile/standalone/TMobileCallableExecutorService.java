package com.tmobile.standalone;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class TMobileCallableExecutorService implements Callable<List<String>> {
    List<String> duplicateList;
    //List<HAProxyConfigModel> propList;
    List<String> keyList;
    /*
    public TMobileCallableExecutorService(List<String> duplicateList, List<HAProxyConfigModel> propList) {
        this.duplicateList = duplicateList;
        this.propList = propList;
    }
    */

    public TMobileCallableExecutorService(List<String> duplicateList, List<String> keyList) {
        this.duplicateList = duplicateList;
        this.keyList = keyList;
    }

    @Override
    public List<String> call() {
        List<String> dList = new ArrayList<>();
        for (String i1 : duplicateList) {
            //for (int i = 0; i < propList.size(); i++) {
            for (int i = 0; i < keyList.size(); i++) {
                //if (i1.equals(propList.get(i).getKey())) {
                if (i1.equals(keyList.get(i))) {
                    dList.add(i1 + ":" + i);
                }
            }
        }
        return dList;
    }
}
