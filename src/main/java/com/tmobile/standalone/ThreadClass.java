package com.tmobile.standalone;



import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadClass {
        static ExecutorService executor = Executors.newFixedThreadPool(3);
        List<HAProxyConfigModel> l1;
        List<HAProxyConfigModel> l2;
        List<HAProxyConfigModel> l3;
        List<HAProxyConfigModel> l4;
        List<HAProxyConfigModel> l5;
        //Map<Integer, List<HAProxyConfigModel>> map = new TreeMap<>();

    public ThreadClass(List<HAProxyConfigModel> l1,
                       List<HAProxyConfigModel> l2,
                       List<HAProxyConfigModel> l3,
                       List<HAProxyConfigModel> l4,
                       List<HAProxyConfigModel> l5) {
        this.l1 = l1;
        this.l2 = l2;
        this.l3 = l3;
        this.l4 = l4;
        this.l5 = l5;
    }

    public static void multiThread() {
            Runnable thread1 = () -> {
                // perform some operation
                System.out.println(Thread.currentThread().getName());
            };
            Runnable thread2 = () -> {
                // perform some operation
                System.out.println(Thread.currentThread().getName());
            };
            Runnable thread3 = new Runnable() {
                @Override
                public void run() {
                    // perform some operation
                    System.out.println(Thread.currentThread().getName());
                }
            };
            executor.execute(thread1);
            executor.execute(thread2);
            executor.execute(thread3);
            executor.shutdown();
        }
}