package com.tmobile.standalone;


import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class TMobileCallableHelper {

    public List<String> executeCallable(Set<String> duplicateList, List<HAProxyConfigModel> propList){
        List<String> dList = new ArrayList<>();
        List<String> mainList = new ArrayList<String>();
        mainList.addAll(duplicateList);
        List<List<String>> parts = new ArrayList<>();
        int L = 20;
        final int N = mainList.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<>(mainList.subList(i, Math.min(N, i + L))));
        }
        System.out.println("Total Split Parts :" + parts.size());
        ExecutorService executor  = Executors.newFixedThreadPool(20);
        List<TMobileCallableExecutorService> taskList = new LinkedList<>();
        //List<TMobileCallableExecutorService> taskList = new ArrayList<>();

        List<String> keyList = new ArrayList<>();
        for(HAProxyConfigModel model : propList){
            keyList.add(model.getKey());
        }

        TMobileCallableExecutorService task = new TMobileCallableExecutorService(mainList, keyList);
        //taskList.add(task);
        return task.call();

        /*
        for (List<String> partialKeysList : parts) {
            TMobileCallableExecutorService task = new TMobileCallableExecutorService(partialKeysList, keyList);
            task.call();
            taskList.add(task);
        }
        */
        /*
        //Execute all tasks and get reference to Future objects
        List<Future<List<String>>> dupList = null;
        try {
            dupList = executor.invokeAll(taskList);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        executor.shutdown();

        System.out.println("\n========Printing the results======");
        System.out.println(executor.isShutdown());

        for (int i = 0; i < dupList.size(); i++) {
            Future<List<String>> future = dupList.get(i);
            try {
                List<String> result = future.get();
                dList.addAll(result);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        return dList;
         */
    }

}
