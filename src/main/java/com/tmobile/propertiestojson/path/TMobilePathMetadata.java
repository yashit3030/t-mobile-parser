package com.tmobile.propertiestojson.path;

import com.tmobile.propertiestojson.TMobilePropertyArrayHelper;
import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import lombok.Data;

import static com.tmobile.parser.utils.TMobileConstants.EMPTY_STRING;
import static com.tmobile.parser.utils.TMobileConstants.NORMAL_DOT;


@Data
public class TMobilePathMetadata {

    private static final String NUMBER_PATTERN = "([1-9]\\d*)|0";
    public static final String INDEXES_PATTERN = "\\s*(\\[\\s*((" + NUMBER_PATTERN + ")|\\*)\\s*]\\s*)+";

    private static final String WORD_PATTERN = "(.)*";

    private final String originalPropertyKey;
    private TMobilePathMetadata parent;
    private String fieldName;
    private String originalFieldName;
    private TMobilePathMetadata child;
    private TMobilePropertyArrayHelper propertyArrayHelper;
    private Object rawValue;
    private TMobileAbstractJsonType jsonValue;

    public TMobilePathMetadata(String originalPropertyKey) {
        this.originalPropertyKey = originalPropertyKey;
    }

    public boolean isLeaf() {
        return child == null;
    }

    public boolean isRoot() {
        return parent == null;
    }

    public String getCurrentFullPath() {
        return parent == null ? originalFieldName : parent.getCurrentFullPath() + NORMAL_DOT + originalFieldName;
    }

    public TMobilePathMetadata getLeaf() {
        TMobilePathMetadata current = this;
        while (current.getChild() != null) {
            current = current.getChild();
        }
        return current;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
        if (fieldName.matches(WORD_PATTERN + INDEXES_PATTERN)) {
            propertyArrayHelper = new TMobilePropertyArrayHelper(fieldName);
            this.fieldName = propertyArrayHelper.getArrayFieldName();
        }
    }

    public void setRawValue(Object rawValue) {
        if (!isLeaf()) {
            throw new RuntimeException("Cannot set value for not leaf: " + getCurrentFullPath());
        }
        this.rawValue = rawValue;
    }

    public String getOriginalPropertyKey() {
        return originalPropertyKey;
    }

    public TMobilePathMetadata getRoot() {
        TMobilePathMetadata current = this;
        while (current.getParent() != null) {
            current = current.getParent();
        }
        return current;
    }

    public String getCurrentFullPathWithoutIndexes() {
        String parentFullPath = isRoot() ? EMPTY_STRING : getParent().getCurrentFullPath() + NORMAL_DOT;
        return parentFullPath + getFieldName();
    }

    public void setJsonValue(TMobileAbstractJsonType jsonValue) {
        if (!isLeaf()) {
            throw new RuntimeException("Cannot set value for not leaf: " + getCurrentFullPath());
        }
        this.jsonValue = jsonValue;
    }

    public TMobileAbstractJsonType getJsonValue() {
        return jsonValue;
    }

    @Override
    public String toString() {
        return "field='" + fieldName + '\'' +
               ", rawValue=" + rawValue +
               ", fullPath='" + getCurrentFullPath() + '}';
    }

    public boolean isArrayField() {
        return propertyArrayHelper != null;
    }
    // Getter & Setter
    public static String getNumberPattern() {
        return NUMBER_PATTERN;
    }

    public static String getIndexesPattern() {
        return INDEXES_PATTERN;
    }

    public static String getWordPattern() {
        return WORD_PATTERN;
    }

    public TMobilePathMetadata getParent() {
        return parent;
    }

    public void setParent(TMobilePathMetadata parent) {
        this.parent = parent;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getOriginalFieldName() {
        return originalFieldName;
    }

    public void setOriginalFieldName(String originalFieldName) {
        this.originalFieldName = originalFieldName;
    }

    public TMobilePathMetadata getChild() {
        return child;
    }

    public void setChild(TMobilePathMetadata child) {
        this.child = child;
    }

    public TMobilePropertyArrayHelper getPropertyArrayHelper() {
        return propertyArrayHelper;
    }

    public void setPropertyArrayHelper(TMobilePropertyArrayHelper propertyArrayHelper) {
        this.propertyArrayHelper = propertyArrayHelper;
    }

    public Object getRawValue() {
        return rawValue;
    }

    // Getter & Setter

}
