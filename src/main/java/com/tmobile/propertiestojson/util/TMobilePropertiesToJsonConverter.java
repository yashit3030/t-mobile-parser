package com.tmobile.propertiestojson.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.tmobile.propertiestojson.TMobileAlgorithmType;
import com.tmobile.propertiestojson.TMobileJsonObjectsTraverseResolver;
import com.tmobile.propertiestojson.helper.TMobilePropertyKeysOrderResolver;
import com.tmobile.propertiestojson.object.TMobileObjectJsonType;
import com.tmobile.propertiestojson.path.TMobilePathMetadata;
import com.tmobile.propertiestojson.path.TMobilePathMetadataBuilder;
import com.tmobile.propertiestojson.resolvers.TMobileArrayJsonTypeResolver;
import com.tmobile.propertiestojson.resolvers.TMobileJsonTypeResolver;
import com.tmobile.propertiestojson.resolvers.TMobileObjectJsonTypeResolver;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;
import com.tmobile.propertiestojson.resolvers.primitives.TMobilePrimitiveJsonTypeResolver;
import com.tmobile.propertiestojson.resolvers.primitives.TMobileStringJsonTypeResolver;
import com.tmobile.propertiestojson.resolvers.primitives.adapter.TMobilePrimitiveJsonTypeResolverToNewApiAdapter;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileNullToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileObjectToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileStringToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToConcreteObjectResolver;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToEmptyStringResolver;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToJsonNullReferenceResolver;
import com.tmobile.propertiestojson.util.exception.TMobileParsePropertiesException;
import com.tmobile.propertiestojson.util.exception.TMobileReadInputException;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

import static com.tmobile.parser.utils.TMobileConstants.ARRAY_START_SIGN;
import static com.tmobile.propertiestojson.resolvers.primitives.object.TMobileNullToJsonTypeConverter.NULL_TO_JSON_RESOLVER;
import static com.tmobile.propertiestojson.resolvers.primitives.object.TMobileStringToJsonTypeConverter.STRING_TO_JSON_RESOLVER;
import static com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToEmptyStringResolver.EMPTY_TEXT_RESOLVER;
import static com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToJsonNullReferenceResolver.TEXT_TO_NULL_JSON_RESOLVER;
import static com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToStringResolver.TO_STRING_RESOLVER;
import static com.tmobile.propertiestojson.util.TMobilePropertiesToJsonConverterBuilder.TO_JSON_TYPE_CONVERTERS;
import static com.tmobile.propertiestojson.util.TMobilePropertiesToJsonConverterBuilder.TO_OBJECT_RESOLVERS;
import static com.tmobile.propertiestojson.util.exception.TMobileParsePropertiesException.*;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;


public final class TMobilePropertiesToJsonConverter {

    private final TMobileNullToJsonTypeConverter nullToJsonConverter;
    private final TMobileTextToJsonNullReferenceResolver textToJsonNullResolver;
    private final TMobileTextToEmptyStringResolver textToEmptyStringResolver;

    private final Map<TMobileAlgorithmType, TMobileJsonTypeResolver> algorithms = new HashMap<>();
    private final TMobilePrimitiveJsonTypesResolver primitiveResolvers;

    private TMobilePropertyKeysOrderResolver propertyKeysOrderResolver = new TMobilePropertyKeysOrderResolver();

    /**
     * Default implementation of json primitive type resolvers.
     */
    public TMobilePropertiesToJsonConverter() {
        this(TO_OBJECT_RESOLVERS, TO_JSON_TYPE_CONVERTERS);
    }

    /**
     * This is deprecated constructor and will be remove in 6.0.0 version.
     * Please migrate you resolvers for new interfaces and build PropertiesToJsonConverter instance through {@link PropertiesToJsonConverterBuilder}
     *
     * @param customPrimitiveResolvers ordered list
     */
    @Deprecated
    public TMobilePropertiesToJsonConverter(TMobilePrimitiveJsonTypeResolver... customPrimitiveResolvers) {
        this(convertToNewResolvers(customPrimitiveResolvers), convertToNewConverters(customPrimitiveResolvers));
    }

    private static List<TMobileObjectToJsonTypeConverter> convertToNewConverters(TMobilePrimitiveJsonTypeResolver... customPrimitiveResolvers) {
        validateTypeResolverOrder(customPrimitiveResolvers);

        return Arrays.stream(customPrimitiveResolvers)
                     .map(TMobilePrimitiveJsonTypeResolverToNewApiAdapter::new)
                     .collect(Collectors.toList());
    }

    private static List<TMobileTextToConcreteObjectResolver> convertToNewResolvers(TMobilePrimitiveJsonTypeResolver... customPrimitiveResolvers) {
        return Arrays.stream(customPrimitiveResolvers)
                     .map(TMobilePrimitiveJsonTypeResolverToNewApiAdapter::new)
                     .collect(Collectors.toList());
    }

    public TMobilePropertiesToJsonConverter(List<TMobileTextToConcreteObjectResolver> toObjectsResolvers,
                                     List<TMobileObjectToJsonTypeConverter> toJsonTypeResolvers) {
        this(toObjectsResolvers, toJsonTypeResolvers, NULL_TO_JSON_RESOLVER, TEXT_TO_NULL_JSON_RESOLVER, EMPTY_TEXT_RESOLVER, false);
    }

    public TMobilePropertiesToJsonConverter(List<TMobileTextToConcreteObjectResolver> toObjectsResolvers,
                                     List<TMobileObjectToJsonTypeConverter> toJsonTypeResolvers,
                                     TMobileNullToJsonTypeConverter nullToJsonConverter,
                                     TMobileTextToJsonNullReferenceResolver textToJsonNullResolver,
                                     TMobileTextToEmptyStringResolver textToEmptyStringResolver,
                                     Boolean skipNull) {

        this.nullToJsonConverter = nullToJsonConverter;
        this.textToJsonNullResolver = textToJsonNullResolver;
        this.textToEmptyStringResolver = textToEmptyStringResolver;

        validateTypeResolverOrder(toJsonTypeResolvers);
        this.primitiveResolvers = new TMobilePrimitiveJsonTypesResolver(buildAllToObjectResolvers(toObjectsResolvers),
                                                                 buildAllToJsonResolvers(toJsonTypeResolvers),
                                                                 skipNull,
                                                                 nullToJsonConverter);
        algorithms.put(TMobileAlgorithmType.OBJECT, new TMobileObjectJsonTypeResolver());
        algorithms.put(TMobileAlgorithmType.PRIMITIVE, this.primitiveResolvers);
        algorithms.put(TMobileAlgorithmType.ARRAY, new TMobileArrayJsonTypeResolver());
    }

    /**
     * Merged list of TextToConcreteObjectResolver instances for first conversion phase
     *
     * @param resolvers provided by user
     * @return list
     */
    private List<TMobileTextToConcreteObjectResolver> buildAllToObjectResolvers(List<TMobileTextToConcreteObjectResolver> resolvers) {
        List<TMobileTextToConcreteObjectResolver> allResolvers = new ArrayList<>();
        allResolvers.add(textToJsonNullResolver);
        allResolvers.add(textToEmptyStringResolver);
        allResolvers.addAll(resolvers);
        allResolvers.add(TO_STRING_RESOLVER);
        return allResolvers;
    }

    /**
     * Merged list of ObjectToJsonTypeConverter instances for second conversion phase
     *
     * @param toJsonTypeResolvers provided by user
     * @return list
     */
    private List<TMobileObjectToJsonTypeConverter> buildAllToJsonResolvers(List<TMobileObjectToJsonTypeConverter> toJsonTypeResolvers) {
        List<TMobileObjectToJsonTypeConverter> mergedToJsonTypeConverters = new ArrayList<>(toJsonTypeResolvers);
        mergedToJsonTypeConverters.add(STRING_TO_JSON_RESOLVER);
        mergedToJsonTypeConverters.add(nullToJsonConverter);
        return mergedToJsonTypeConverters;
    }

    /**
     * It generates Json from properties file stored in provided path as string.
     * Every property value will tries resolve to concrete object by given resolvers...
     * It will try convert to some object (number, boolean, list etc, depends on generic type of given {@link TextToConcreteObjectResolver}) from string value through method:
     * {@link TextToConcreteObjectResolver#returnObjectWhenCanBeResolved(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey)}
     * The order of resolvers is important because on that depends on which resolver as first will convert from string to some given object...
     * <p>
     * Next will looks for sufficient converter, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param pathToFile path to File
     * @return simple String with json
     * @throws ReadInputException       when cannot find file
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */
    public String convertPropertiesFromFileToJson(String pathToFile) throws TMobileReadInputException, TMobileParsePropertiesException {
        return convertPropertiesFromFileToJson(new File(pathToFile));
    }

    /**
     * It generates Json from properties file stored in provided path as string and will converts only included keys or parts of property keys provided by second parameter.
     * Every property value will tries resolve to concrete object by given resolvers...
     * It will try convert to some object (number, boolean, list etc, depends on generic type of given {@link TextToConcreteObjectResolver}) from string value through method:
     * {@link TextToConcreteObjectResolver#returnObjectWhenCanBeResolved(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey)}
     * The order of resolvers is important because on that depends on which resolver as first will convert from string to some given object...
     * <p>
     * Next will looks for sufficient converter, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param pathToFile        path to File
     * @param includeDomainKeys domain head keys which should be parsed to json <br>
     *                          example properties:<br>
     *                          object1.field1=value1<br>
     *                          object1.field2=value2<br>
     *                          someObject2.field2=value3<br>
     *                          filter "object1"<br>
     *                          will parse only nested domain for "object1"<br>
     * @return simple String with json
     * @throws ReadInputException       when cannot find file
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */
    public String convertPropertiesFromFileToJson(String pathToFile, String... includeDomainKeys) throws TMobileReadInputException, TMobileParsePropertiesException {
        return convertPropertiesFromFileToJson(new File(pathToFile), includeDomainKeys);
    }

    /**
     * It generates Json from properties file stored in provided File.
     * Every property value will tries resolve to concrete object by given resolvers...
     * It will try convert to some object (number, boolean, list etc, depends on generic type of given {@link TextToConcreteObjectResolver}) from string value through method:
     * {@link TextToConcreteObjectResolver#returnObjectWhenCanBeResolved(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey)}
     * The order of resolvers is important because on that depends on which resolver as first will convert from string to some given object...
     * <p>
     * Next will looks for sufficient converter, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param file file with properties
     * @return simple String with json
     * @throws ReadInputException       when cannot find file
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */
    public String convertPropertiesFromFileToJson(File file) throws TMobileReadInputException, TMobileParsePropertiesException {
        try {
            InputStream targetStream = new FileInputStream(file);
            return convertToJson(targetStream);
        } catch(FileNotFoundException e) {
            throw new TMobileReadInputException(e);
        }
    }

    /**
     * It generates Json from properties file stored in provided File and will converts only included keys or parts of property keys provided by second parameter.
     * Every property value will tries resolve to concrete object by given resolvers...
     * It will try convert to some object (number, boolean, list etc, depends on generic type of given {@link TextToConcreteObjectResolver}) from string value through method:
     * {@link TextToConcreteObjectResolver#returnObjectWhenCanBeResolved(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey)}
     * The order of resolvers is important because on that depends on which resolver as first will convert from string to some given object...
     * <p>
     * Next will looks for sufficient converter, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param file              file with properties
     * @param includeDomainKeys domain head keys which should be parsed to json <br>
     *                          example properties:<br>
     *                          object1.field1=value1<br>
     *                          object1.field2=value2<br>
     *                          someObject2.field2=value3<br>
     *                          filter "object1"<br>
     *                          will parse only nested domain for "object1"<br>
     * @return simple String with json
     * @throws ReadInputException       when cannot find file
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */
    public String convertPropertiesFromFileToJson(File file, String... includeDomainKeys) throws TMobileReadInputException, TMobileParsePropertiesException {
        try {
            InputStream targetStream = new FileInputStream(file);
            return convertToJson(targetStream, includeDomainKeys);
        } catch(FileNotFoundException e) {
            throw new TMobileReadInputException(e);
        }
    }

    /**
     * It generates Json from properties stored in provided InputStream.
     * Every property value will tries resolve to concrete object by given resolvers...
     * It will try convert to some object (number, boolean, list etc, depends on generic type of given {@link TextToConcreteObjectResolver}) from string value through method:
     * {@link TextToConcreteObjectResolver#returnObjectWhenCanBeResolved(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey)}
     * The order of resolvers is important because on that depends on which resolver as first will convert from string to some given object...
     * <p>
     * Next will looks for sufficient converter, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param inputStream InputStream with properties
     * @return simple String with json
     * @throws ReadInputException       when cannot find file
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */
    public String convertToJson(InputStream inputStream) throws TMobileReadInputException, TMobileParsePropertiesException {
        return inputStreamToProperties(inputStream);
    }

    /**
     * It generates Json from properties stored in provided InputStream and will converts only included keys or parts of property keys provided by second parameter.
     * Every property value will tries resolve to concrete object by given resolvers...
     * It will try convert to some object (number, boolean, list etc, depends on generic type of given {@link TextToConcreteObjectResolver}) from string value through method:
     * {@link TextToConcreteObjectResolver#returnObjectWhenCanBeResolved(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey)}
     * The order of resolvers is important because on that depends on which resolver as first will convert from string to some given object...
     * <p>
     * Next will looks for sufficient converter, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param inputStream       InputStream with properties
     * @param includeDomainKeys domain head keys which should be parsed to json <br>
     *                          example properties:<br>
     *                          object1.field1=value1<br>
     *                          object1.field2=value2<br>
     *                          someObject2.field2=value3<br>
     *                          filter "object1"<br>
     *                          will parse only nested domain for "object1"<br>
     * @return simple String with json
     * @throws ReadInputException       when cannot find file
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */
    public String convertToJson(InputStream inputStream, String... includeDomainKeys) throws TMobileReadInputException, TMobileParsePropertiesException {
        return "";//convertToJson(inputStreamToProperties(inputStream), includeDomainKeys);
    }

    /**
     * It generates Json from given Java Properties instance.
     * If property value will be string then will not try convert it to another type.
     * <p>
     * It will only looks for sufficient resolver, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param properties Java Properties
     * @return simple String with json
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */
    public String convertToJson(Properties properties) throws TMobileParsePropertiesException {
        for(Map.Entry<Object, Object> entry : properties.entrySet()) {
            if(!(entry.getKey() instanceof String)) {
                throw new TMobileParsePropertiesException(format(PROPERTY_KEY_NEEDS_TO_BE_STRING_TYPE,
                                                          entry.getKey().getClass(),
                                                          entry.getKey() == null ? "null" : entry.getKey()));
            }
        }
        return convertFromValuesAsObjectMap(propertiesToMap(properties));
    }

    /**
     * It generates Json from given Java Properties instance and will converts only included keys or parts of property keys provided by second parameter.
     * If property value will be string then will not try convert it to another type.
     * <p>
     * It will only looks for sufficient resolver, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param properties        Java Properties
     * @param includeDomainKeys domain head keys which should be parsed to json <br>
     *                          example properties:<br>
     *                          object1.field1=value1<br>
     *                          object1.field2=value2<br>
     *                          someObject2.field2=value3<br>
     *                          filter "object1"<br>
     *                          will parse only nested domain for "object1"<br>
     * @return Simple String with json
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */
    public String convertToJson(Properties properties, String... includeDomainKeys) throws TMobileParsePropertiesException {
        return convertFromValuesAsObjectMap(propertiesToMap(properties), includeDomainKeys);
    }

    /**
     * It generates Json from given Map&lt;String,String&gt; instance.
     * Every property value will tries resolve to concrete object by given resolvers...
     * It will try convert to some object (number, boolean, list etc, depends on generic type of given {@link TextToConcreteObjectResolver}) from string value through method:
     * {@link TextToConcreteObjectResolver#returnObjectWhenCanBeResolved(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey)}
     * The order of resolvers is important because on that depends on which resolver as first will convert from string to some given object...
     * <p>
     * Next will looks for sufficient converter, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param properties Java Map with properties
     * @return simple String with json
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */

    public String convertToJson(Map<String, String> properties) throws TMobileParsePropertiesException {
        return convertFromValuesAsObjectMap(stringValueMapToObjectValueMap(properties));

    }

    /**
     * It generates Json from given Map&lt;String,String&gt; instance and will converts only included keys or parts of property keys provided by second parameter.
     * Every property value will tries resolve to concrete object by given resolvers...
     * It will try convert to some object (number, boolean, list etc, depends on generic type of given {@link TextToConcreteObjectResolver}) from string value through method:
     * {@link TextToConcreteObjectResolver#returnObjectWhenCanBeResolved(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey)}
     * The order of resolvers is important because on that depends on which resolver as first will convert from string to some given object...
     * <p>
     * Next will looks for sufficient converter, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param properties        Java Map with properties
     * @param includeDomainKeys domain head keys which should be parsed to json <br>
     *                          example properties:<br>
     *                          object1.field1=value1<br>
     *                          object1.field2=value2<br>
     *                          someObject2.field2=value3<br>
     *                          filter "object1"<br>
     *                          will parse only nested domain for "object1"<br>
     * @return simple String with json
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */

    public String convertToJson(Map<String, String> properties, String... includeDomainKeys) throws TMobileParsePropertiesException {
        return convertFromValuesAsObjectMap(stringValueMapToObjectValueMap(properties), includeDomainKeys);
    }

    /**
     * It generates Json given Map&lt;String,Object&gt; instance.
     * If property value will be string then will not try convert it to another type.
     * <p>
     * It will only looks for sufficient resolver, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param properties Java Map with properties
     * @return simple String with json
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */

    public String convertFromValuesAsObjectMap(Map<String, String> properties) throws TMobileParsePropertiesException {
        TMobileObjectJsonType coreObjectJsonType = new TMobileObjectJsonType();
        for(String propertyKey : getAllKeysFromProperties(properties)) {
            addFieldsToJsonObject(properties, coreObjectJsonType, propertyKey);
        }
        return prettifyOfJson(coreObjectJsonType.toStringJson());
    }

    private static String prettifyOfJson(String json) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(json);
        return gson.toJson(je);
    }

    /**
     * It generates Json given Map&lt;String,Object&gt; instance and will converts only included keys or parts of property keys provided by second parameter.
     * If property value will be string then will not try convert it to another type.
     * <p>
     * It will only looks for sufficient resolver, firstly will looks for exactly match class type,
     * if not then will looks for closets parent class or parent interface.
     * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
     * If will find only closets super interfaces (at the same level) then will throw exception...
     * after successful found resolver it converts from given object to some instance which extends AbstractJsonType
     * through method {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey)}
     *
     * @param properties        Java Map with properties
     * @param includeDomainKeys domain head keys which should be parsed to json <br>
     *                          example properties:<br>
     *                          object1.field1=value1<br>
     *                          object1.field2=value2<br>
     *                          someObject2.field2=value3<br>
     *                          filter "object1"<br>
     *                          will parse only nested domain for "object1"<br>
     * @return simple String with json
     * @throws ParsePropertiesException when structure of properties is not compatible with json structure
     */
    public String convertFromValuesAsObjectMap(Map<String, Object> properties, String... includeDomainKeys) throws TMobileParsePropertiesException {
        Map<String, Object> filteredProperties = new HashMap<>();
        for(String key : properties.keySet()) {
            for(String requiredKey : includeDomainKeys) {
                checkKey(properties, filteredProperties, key, requiredKey);
            }
        }
        return convertFromValuesAsObjectMap(filteredProperties);
    }

    /**
     * It change implementation of order gathering keys from properties
     *
     * @param propertyKeysOrderResolver another implementation of get ordered properties keys
     */
    public void setPropertyKeysOrderResolver(TMobilePropertyKeysOrderResolver propertyKeysOrderResolver) {
        requireNonNull(propertyKeysOrderResolver);
        this.propertyKeysOrderResolver = propertyKeysOrderResolver;
    }

    private static void checkKey(Map<String, Object> properties, Map<String, Object> filteredProperties, String key, String requiredKey) {
        if(key.equals(requiredKey) || (key.startsWith(requiredKey) && keyIsCompatibleWithRequiredKey(requiredKey, key))) {
            filteredProperties.put(key, properties.get(key));
        }
    }

    private static boolean keyIsCompatibleWithRequiredKey(String requiredKey, String key) {
        String testedChar = key.substring(requiredKey.length(), requiredKey.length() + 1);
        if(testedChar.equals(ARRAY_START_SIGN) || testedChar.equals(".")) {
            return true;
        }
        return false;
    }

    private String inputStreamToProperties(InputStream inputStream) throws TMobileReadInputException {
        Properties propertiesWithConvertedValues = new Properties();
        OrderedProperties properties = new OrderedProperties();
        try {
            properties.load(inputStream);
            for(String property : getAllKeysFromProperties(propertiesToMap(properties))) {
                Object object = properties.getProperty(property);//primitiveResolvers.getResolvedObject((String) properties.get(property), property);
                propertiesWithConvertedValues.put(property, object);
            }
        } catch(IOException e) {
            throw new TMobileReadInputException(e);
        }
        Properties propertiesWithConvertedValues1 = new Properties();
        propertiesWithConvertedValues1.putAll(properties.properties);

        return convertFromValuesAsObjectMap(properties.properties);

    }

    private void addFieldsToJsonObject(Map<String, String> properties, TMobileObjectJsonType coreObjectJsonType, String propertyKey) {
        TMobilePathMetadata rootPathMetaData = TMobilePathMetadataBuilder.createRootPathMetaData(propertyKey);
        TMobileJsonObjectsTraverseResolver trav = new TMobileJsonObjectsTraverseResolver(algorithms, properties, propertyKey, rootPathMetaData, coreObjectJsonType);
        trav.initializeFieldsInJson();
    }

    private List<String> getAllKeysFromProperties(Map<String, ?> properties) {
        return propertyKeysOrderResolver.getKeysInExpectedOrder(properties);
    }


    private static Map<String, Object> propertiesToMap(Properties properties) {
        Map<String, Object> map = new HashMap<>();
        for(Map.Entry<Object, Object> property : properties.entrySet()) {
            map.put(property.getKey().toString(), property.getValue());
        }
        return map;
    }
    private static Map<String, Object> propertiesToMap(OrderedProperties properties) {
        Map<String, Object> map = new LinkedHashMap<>();
        for(Map.Entry<String, String> property : properties.entrySet()) {
            map.put(property.getKey(), property.getValue());
        }
        return map;
    }
    private Map<String, Object> stringValueMapToObjectValueMap(Map<String, String> properties) {
        Map<String, Object> map = new HashMap<>();
        for(String property : getAllKeysFromProperties(properties)) {
            Object object = primitiveResolvers.getResolvedObject(properties.get(property), property);
            map.put(property, object);
        }
        return map;
    }

    private static void validateTypeResolverOrder(TMobilePrimitiveJsonTypeResolver... primitiveResolvers) {
        List<TMobilePrimitiveJsonTypeResolver> resolvers = asList(primitiveResolvers);
        boolean containStringResolverType = false;
        for(TMobilePrimitiveJsonTypeResolver resolver : resolvers) {
            if(resolver.getClass().equals(TMobileStringJsonTypeResolver.class)) {
                containStringResolverType = true;
            }
        }
        if(containStringResolverType) {
            TMobilePrimitiveJsonTypeResolver lastResolver = resolvers.get(resolvers.size() - 1);
            if(!(lastResolver.getClass().equals(TMobileStringJsonTypeResolver.class))) {
                throw new TMobileParsePropertiesException(STRING_RESOLVER_AS_NOT_LAST);
            }
        }
    }

    private static void validateTypeResolverOrder(List<TMobileObjectToJsonTypeConverter> resolvers) {
        boolean containStringResolverType = false;
        for(TMobileObjectToJsonTypeConverter<?> resolver : resolvers) {
            if(resolver.getClass().equals(TMobileStringToJsonTypeConverter.class)) {
                containStringResolverType = true;
            }
        }
        if(containStringResolverType) {
            TMobileObjectToJsonTypeConverter<?> lastResolver = resolvers.get(resolvers.size() - 1);
            if(!(lastResolver.getClass().equals(TMobileStringToJsonTypeConverter.class))) {
                throw new TMobileParsePropertiesException(STRING__TO_JSON_RESOLVER_AS_NOT_LAST);
            }
        }
    }
}
