package com.tmobile.propertiestojson.util;

public class TMobileStringToJsonStringWrapper {

    private static final String JSON_STRING_SCHEMA = "\"%s\"";

    public static String wrap(String textToWrap) {
        return String.format(JSON_STRING_SCHEMA, textToWrap.replace("\"","\\\""));
    }

}
