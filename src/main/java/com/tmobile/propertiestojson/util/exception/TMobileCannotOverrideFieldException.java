package com.tmobile.propertiestojson.util.exception;

import com.google.common.annotations.VisibleForTesting;
import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;


public class TMobileCannotOverrideFieldException extends RuntimeException {

    private static final String CANNOT_OVERRIDE_VALUE = "Cannot override value at path: '%s', current value is: '%s', problematic property key: '%s'";

    public TMobileCannotOverrideFieldException(String currentPath, TMobileAbstractJsonType currentValue, String propertyKey) {
        this(currentPath, currentValue.toString(), propertyKey);
    }

    @VisibleForTesting
    public TMobileCannotOverrideFieldException(String currentPath, String currentValue, String propertyKey) {
        super(String.format(CANNOT_OVERRIDE_VALUE, currentPath, currentValue, propertyKey));
    }
}
