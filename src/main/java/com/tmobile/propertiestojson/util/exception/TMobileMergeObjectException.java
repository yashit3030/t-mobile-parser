package com.tmobile.propertiestojson.util.exception;

import com.google.common.annotations.VisibleForTesting;
import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.path.TMobilePathMetadata;


import static java.lang.String.format;

public class TMobileMergeObjectException extends RuntimeException {
    public TMobileMergeObjectException(TMobileAbstractJsonType oldJsonElement, TMobileAbstractJsonType elementToAdd, TMobilePathMetadata currentPathMetadata) {
        this(oldJsonElement.toStringJson(), elementToAdd.toStringJson(), currentPathMetadata);
    }

    @VisibleForTesting
    public TMobileMergeObjectException(String oldJsonElementValue, String elementToAddValue, TMobilePathMetadata currentPathMetadata) {
        super(format("Cannot merge objects with different types:%n Old object: %s%n New object: %s%n problematic key: '%s'%n with value: %s",
                     oldJsonElementValue, elementToAddValue, currentPathMetadata.getOriginalPropertyKey(), currentPathMetadata.getRawValue()));
    }
}
