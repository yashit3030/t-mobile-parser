package com.tmobile.propertiestojson.util;



import com.tmobile.propertiestojson.resolvers.primitives.object.*;
import com.tmobile.propertiestojson.resolvers.primitives.string.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.tmobile.propertiestojson.resolvers.primitives.object.TMobileNullToJsonTypeConverter.NULL_TO_JSON_RESOLVER;
import static com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToEmptyStringResolver.EMPTY_TEXT_RESOLVER;
import static com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToJsonNullReferenceResolver.TEXT_TO_NULL_JSON_RESOLVER;


/**
 * Builder class for PropertiesToJsonConverter
 */
public class TMobilePropertiesToJsonConverterBuilder {

    static final List<TMobileTextToConcreteObjectResolver> TO_OBJECT_RESOLVERS = defaultResolvers();
    static final List<TMobileObjectToJsonTypeConverter> TO_JSON_TYPE_CONVERTERS = defaultConverters();

    /**
     * Default list of resolvers from text to java Object...
     * Order here is important.
     *
     * @return list
     */
    static List<TMobileTextToConcreteObjectResolver> defaultResolvers() {
        List<TMobileTextToConcreteObjectResolver> toObjectResolvers = new ArrayList<>();
        toObjectResolvers.add(new TMobileTextToElementsResolver());
        toObjectResolvers.add(new TMobileTextToObjectResolver());
        toObjectResolvers.add(new TMobileTextToNumberResolver());
        toObjectResolvers.add(new TMobileTextToCharacterResolver());
        toObjectResolvers.add(new TMobileTextToBooleanResolver());
        return Collections.unmodifiableList(toObjectResolvers);
    }

    /**
     * Default list of converters from java Object to some instance of {@link AbstractJsonType}
     *
     * @return list
     */
    static List<TMobileObjectToJsonTypeConverter> defaultConverters() {
        List<TMobileObjectToJsonTypeConverter> toJsonTypeConverters = new ArrayList<>();
        toJsonTypeConverters.add(new TMobileElementsToJsonTypeConverter());
        toJsonTypeConverters.add(new TMobileSuperObjectToJsonTypeConverter());
        toJsonTypeConverters.add(new TMobileNumberToJsonTypeConverter());
        toJsonTypeConverters.add(new TMobileCharacterToJsonTypeConverter());
        toJsonTypeConverters.add(new TMobileBooleanToJsonTypeConverter());
        return Collections.unmodifiableList(toJsonTypeConverters);
    }

    private final List<TMobileTextToConcreteObjectResolver> resolvers = new ArrayList<>();
    private final List<TMobileObjectToJsonTypeConverter> converters = new ArrayList<>();

    private TMobileNullToJsonTypeConverter nullToJsonConverter = NULL_TO_JSON_RESOLVER;
    private TMobileTextToJsonNullReferenceResolver textToJsonNullResolver = TEXT_TO_NULL_JSON_RESOLVER;
    private TMobileTextToEmptyStringResolver textToEmptyStringResolver = EMPTY_TEXT_RESOLVER;
    private boolean skipNul = false;
    private boolean onlyCustomConverters = false;
    private boolean onlyCustomResolvers = false;

    /**
     * Returns new instance of builder.
     *
     * @return builder instance
     */
    public static TMobilePropertiesToJsonConverterBuilder builder() {
        return new TMobilePropertiesToJsonConverterBuilder();
    }

    /**
     * Will build PropertiesToJsonConverter only with instances provided in argument.
     * Order of resolvers has meaning. Order is crucial.
     *
     * @param resolvers it override default list of {@link PropertiesToJsonConverterBuilder#TO_OBJECT_RESOLVERS}
     * @return builder instance
     */
    public TMobilePropertiesToJsonConverterBuilder onlyCustomTextToObjectResolvers(TMobileTextToConcreteObjectResolver... resolvers) {
        onlyCustomResolvers = true;
        this.resolvers.addAll(Arrays.asList(resolvers));
        return this;
    }

    /**
     * Will build PropertiesToJsonConverter with combined list of {@link PropertiesToJsonConverterBuilder#TO_OBJECT_RESOLVERS} and instances provided in argument.
     *
     * @param resolvers added to {@link PropertiesToJsonConverterBuilder#TO_OBJECT_RESOLVERS}
     * @return builder instance
     */
    public TMobilePropertiesToJsonConverterBuilder defaultAndCustomTextToObjectResolvers(TMobileTextToConcreteObjectResolver... resolvers) {
        onlyCustomResolvers = false;
        this.resolvers.addAll(Arrays.asList(resolvers));
        return this;
    }

    /**
     * Will build PropertiesToJsonConverter only with instances provided in argument.
     * Order of converters has meaning only when converters can convert from the same java class.
     *
     * @param converters it override default list of {@link PropertiesToJsonConverterBuilder#TO_JSON_TYPE_CONVERTERS}
     * @return builder instance
     */
    public TMobilePropertiesToJsonConverterBuilder onlyCustomObjectToJsonTypeConverters(TMobileObjectToJsonTypeConverter... converters) {
        onlyCustomConverters = true;
        this.converters.addAll(Arrays.asList(converters));
        return this;
    }

    /**
     * Will build PropertiesToJsonConverter with combined list of {@link PropertiesToJsonConverterBuilder#TO_JSON_TYPE_CONVERTERS} and instances provided in argument.
     *
     * @param converters added to {@link PropertiesToJsonConverterBuilder#TO_JSON_TYPE_CONVERTERS}
     * @return builder instance
     */
    public TMobilePropertiesToJsonConverterBuilder defaultAndCustomObjectToJsonTypeConverters(TMobileObjectToJsonTypeConverter... converters) {
        onlyCustomConverters = false;
        this.converters.addAll(Arrays.asList(converters));
        return this;
    }

    /**
     * It override default conversion of json null reference for something what you need. (return of this instance will be simply concated to json)
     *
     * @param nullToJsonConverter new implementation of NullToJsonTypeConverter
     * @return builder instance
     */
    public TMobilePropertiesToJsonConverterBuilder overrideNullToJsonConverter(TMobileNullToJsonTypeConverter nullToJsonConverter) {
        this.nullToJsonConverter = nullToJsonConverter;
        return this;
    }

    /**
     * It override default behavior for resolving of text "null" or java.lang.String with null reference.
     *
     * @param textToJsonNullResolver new implementation of TextToJsonNullReferenceResolver
     * @return builder instance
     */
    public TMobilePropertiesToJsonConverterBuilder overrideTextToJsonNullResolver(TMobileTextToJsonNullReferenceResolver textToJsonNullResolver) {
        this.textToJsonNullResolver = textToJsonNullResolver;
        return this;
    }

    /**
     * It override default behavior for resolving of empty text in java.lang.String.
     *
     * @param textToEmptyStringResolver new implementation of TextToEmptyStringResolver
     * @return builder instance
     */
    public TMobilePropertiesToJsonConverterBuilder overrideTextToEmptyStringResolver(TMobileTextToEmptyStringResolver textToEmptyStringResolver) {
        this.textToEmptyStringResolver = textToEmptyStringResolver;
        return this;
    }

    /**
     * It will skip every leaf in json object which is null, not skip null in arrays.
     *
     * @return PropertiesToJsonConverterBuilder instance
     */
    public TMobilePropertiesToJsonConverterBuilder skipNulls() {
        skipNul = true;
        return this;
    }

    /**
     * It creates new converter instance.
     *
     * @return instance of PropertiesToJsonConverter
     */
    public TMobilePropertiesToJsonConverter build() {
        List<TMobileObjectToJsonTypeConverter> resultConverters = new ArrayList<>();
        if(onlyCustomConverters) {
            resultConverters.addAll(converters);
        } else {
            resultConverters.addAll(converters);
            resultConverters.addAll(TO_JSON_TYPE_CONVERTERS);
        }

        List<TMobileTextToConcreteObjectResolver> resultResolvers = new ArrayList<>();
        if(onlyCustomResolvers) {
            resultResolvers.addAll(resolvers);
        } else {
            resultResolvers.addAll(resolvers);
            resultResolvers.addAll(TO_OBJECT_RESOLVERS);
        }

        return new TMobilePropertiesToJsonConverter(resultResolvers,
                                             resultConverters,
                                             nullToJsonConverter,
                                             textToJsonNullResolver,
                                             textToEmptyStringResolver,
                                             skipNul);
    }
}
