package com.tmobile.propertiestojson.helper;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;

public class TMobilePropertyKeysOrderResolver {
    public List<String> getKeysInExpectedOrder(Map<String, ?> properties) {
        return Lists.newArrayList(properties.keySet());
    }
}
