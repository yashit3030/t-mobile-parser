package com.tmobile.propertiestojson;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

import static com.tmobile.parser.utils.TMobileConstants.*;
import static com.tmobile.propertiestojson.path.TMobilePathMetadata.INDEXES_PATTERN;


@Getter
public class TMobilePropertyArrayHelper {

    private List<Integer> dimensionalIndexes;
    private String arrayFieldName;

    public TMobilePropertyArrayHelper(String field) {
        arrayFieldName = getNameFromArray(field);
        dimensionalIndexes = getIndexesFromArrayField(field);
    }

    public static String getNameFromArray(String fieldName) {
        return fieldName.replaceFirst(INDEXES_PATTERN + "$", EMPTY_STRING);
    }

    public static List<Integer> getIndexesFromArrayField(String fieldName) {
        String indexesAsText = fieldName.replace(getNameFromArray(fieldName), EMPTY_STRING);
        String[] indexesAsTextArray = indexesAsText
                .replace(ARRAY_START_SIGN, EMPTY_STRING)
                .replace(ARRAY_END_SIGN, SIMPLE_ARRAY_DELIMITER)
                .replaceAll("\\s", EMPTY_STRING)
                .split(SIMPLE_ARRAY_DELIMITER);
        List<Integer> indexes = new ArrayList<>();
        for (String indexAsText : indexesAsTextArray) {
            indexes.add(Integer.valueOf(indexAsText));
        }
        return indexes;
    }
    // Getter & Setter

    public List<Integer> getDimensionalIndexes() {
        return dimensionalIndexes;
    }

    public void setDimensionalIndexes(List<Integer> dimensionalIndexes) {
        this.dimensionalIndexes = dimensionalIndexes;
    }

    public String getArrayFieldName() {
        return arrayFieldName;
    }

    public void setArrayFieldName(String arrayFieldName) {
        this.arrayFieldName = arrayFieldName;
    }
}
