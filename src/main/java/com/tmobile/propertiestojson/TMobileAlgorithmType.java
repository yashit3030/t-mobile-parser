package com.tmobile.propertiestojson;

public enum TMobileAlgorithmType {
    PRIMITIVE, ARRAY, OBJECT
}
