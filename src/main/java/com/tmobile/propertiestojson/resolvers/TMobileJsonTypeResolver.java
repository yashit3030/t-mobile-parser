package com.tmobile.propertiestojson.resolvers;


import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileArrayJsonType;
import com.tmobile.propertiestojson.object.TMobileObjectJsonType;
import com.tmobile.propertiestojson.path.TMobilePathMetadata;
import com.tmobile.propertiestojson.resolvers.transfer.TMobileDataForResolve;

import java.util.Map;



public abstract class TMobileJsonTypeResolver {

    protected Map<String, String> properties;
    protected String propertyKey;
    protected TMobileObjectJsonType currentObjectJsonType;

	protected TMobileArrayJsonType getArrayJsonWhenIsValid(TMobilePathMetadata currentPathMetaData) {
        TMobileAbstractJsonType jsonType = currentObjectJsonType.getField(currentPathMetaData.getFieldName());
        return (TMobileArrayJsonType) jsonType;
    }

    public abstract TMobileObjectJsonType traverse(TMobilePathMetadata currentPathMetaData);

    public final TMobileObjectJsonType traverseOnObjectAndInitByField(TMobileDataForResolve dataForResolve) {
        properties = dataForResolve.getProperties();
        propertyKey = dataForResolve.getPropertiesKey();
        currentObjectJsonType = dataForResolve.getCurrentObjectJsonType();
        return traverse(dataForResolve.getCurrentPathMetaData());
    }
}
