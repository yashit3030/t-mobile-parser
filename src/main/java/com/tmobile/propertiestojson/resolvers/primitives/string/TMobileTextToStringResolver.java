package com.tmobile.propertiestojson.resolvers.primitives.string;


import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.Optional;

public class TMobileTextToStringResolver implements TMobileTextToConcreteObjectResolver<String> {

    public static final TMobileTextToStringResolver TO_STRING_RESOLVER = new TMobileTextToStringResolver();

    @Override
    public Optional<String> returnObjectWhenCanBeResolved(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                          String propertyValue,
                                                          String propertyKey) {
        return Optional.ofNullable(propertyValue);
    }
}
