package com.tmobile.propertiestojson.resolvers.primitives.object;

import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileStringJsonType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.Optional;

public class TMobileCharacterToJsonTypeConverter extends TMobileAbstractObjectToJsonTypeConverter<Character> {

    @Override
    public Optional<TMobileAbstractJsonType> convertToJsonTypeOrEmpty(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                                      Character convertedValue,
                                                                      String propertyKey) {
        return Optional.of(new TMobileStringJsonType(convertedValue.toString()));
    }
}
