package com.tmobile.propertiestojson.resolvers.primitives.string;


import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.Optional;

public class TMobileTextToBooleanResolver implements TMobileTextToConcreteObjectResolver<Boolean> {

    private static final String TRUE = "true";
    private static final String FALSE = "false";

    @Override
    public Optional<Boolean> returnObjectWhenCanBeResolved(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey) {
        if (TRUE.equalsIgnoreCase(propertyValue) || FALSE.equalsIgnoreCase(propertyValue)){
            return Optional.of(getBoolean(propertyValue));
        }
        return Optional.empty();
    }

    private static Boolean getBoolean(String value) {
        return Boolean.valueOf(value);
    }
}
