package com.tmobile.propertiestojson.resolvers.primitives.object;



import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileBooleanJsonType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.Optional;

public class TMobileBooleanToJsonTypeConverter extends TMobileAbstractObjectToJsonTypeConverter<Boolean> {

    @Override
    public Optional<TMobileAbstractJsonType> convertToJsonTypeOrEmpty(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                                      Boolean convertedValue,
                                                                      String propertyKey) {
        return Optional.of(new TMobileBooleanJsonType(convertedValue));
    }
}
