package com.tmobile.propertiestojson.resolvers.primitives.object;

import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileStringJsonType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;


import java.util.Optional;

public class TMobileStringToJsonTypeConverter extends TMobileAbstractObjectToJsonTypeConverter<String> {

    public static final TMobileStringToJsonTypeConverter STRING_TO_JSON_RESOLVER = new TMobileStringToJsonTypeConverter();

    @Override
    public Optional<TMobileAbstractJsonType> convertToJsonTypeOrEmpty(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                                      String convertedValue,
                                                                      String propertyKey) {
        return Optional.of(new TMobileStringJsonType(convertedValue));
    }
}
