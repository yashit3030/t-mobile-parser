package com.tmobile.propertiestojson.resolvers.primitives;


import com.tmobile.propertiestojson.resolvers.primitives.delegator.TMobilePrimitiveJsonTypeDelegatorResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileStringToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToEmptyStringResolver;

@Deprecated
public class TMobileEmptyStringJsonTypeResolver extends TMobilePrimitiveJsonTypeDelegatorResolver<String> {

    public TMobileEmptyStringJsonTypeResolver() {
        super(new TMobileTextToEmptyStringResolver(), new TMobileStringToJsonTypeConverter());
    }
}
