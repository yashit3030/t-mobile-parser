package com.tmobile.propertiestojson.resolvers.primitives.object;



import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileNumberJsonType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.Optional;

public class TMobileNumberToJsonTypeConverter extends TMobileAbstractObjectToJsonTypeConverter<Number> {

    @Override
    public Optional<TMobileAbstractJsonType> convertToJsonTypeOrEmpty(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                                      Number convertedValue,
                                                                      String propertyKey) {
        return Optional.of(new TMobileNumberJsonType(convertedValue));
    }
}
