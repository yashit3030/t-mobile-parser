package com.tmobile.propertiestojson.resolvers.primitives;


import com.tmobile.propertiestojson.object.TMobileJsonNullReferenceType;
import com.tmobile.propertiestojson.resolvers.primitives.delegator.TMobilePrimitiveJsonTypeDelegatorResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileNullToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToJsonNullReferenceResolver;

@Deprecated
public class TMobileJsonNullReferenceTypeResolver extends TMobilePrimitiveJsonTypeDelegatorResolver<TMobileJsonNullReferenceType> {

    public TMobileJsonNullReferenceTypeResolver() {
        super(new TMobileTextToJsonNullReferenceResolver(), new TMobileNullToJsonTypeConverter());
    }
}
