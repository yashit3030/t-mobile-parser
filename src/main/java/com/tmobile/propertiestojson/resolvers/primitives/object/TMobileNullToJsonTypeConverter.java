package com.tmobile.propertiestojson.resolvers.primitives.object;



import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileJsonNullReferenceType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.Optional;

public class TMobileNullToJsonTypeConverter extends TMobileAbstractObjectToJsonTypeConverter<TMobileJsonNullReferenceType> {

    public static final TMobileNullToJsonTypeConverter NULL_TO_JSON_RESOLVER = new TMobileNullToJsonTypeConverter();

    @Override
    public Optional<TMobileAbstractJsonType> convertToJsonTypeOrEmpty(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                                      TMobileJsonNullReferenceType convertedValue,
                                                                      String propertyKey) {
        return Optional.of(convertedValue);
    }
}
