package com.tmobile.propertiestojson.resolvers.primitives;


import com.tmobile.propertiestojson.resolvers.primitives.delegator.TMobilePrimitiveJsonTypeDelegatorResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileStringToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToStringResolver;

@Deprecated
public class TMobileStringJsonTypeResolver extends TMobilePrimitiveJsonTypeDelegatorResolver<String> {

    public TMobileStringJsonTypeResolver() {
        super(new TMobileTextToStringResolver(), new TMobileStringToJsonTypeConverter());
    }
}
