package com.tmobile.propertiestojson.resolvers.primitives.string;


import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.Optional;

import static com.tmobile.propertiestojson.object.TMobileJsonNullReferenceType.NULL_OBJECT;
import static com.tmobile.propertiestojson.object.TMobileJsonNullReferenceType.NULL_VALUE;

public class TMobileTextToJsonNullReferenceResolver implements TMobileTextToConcreteObjectResolver<Object> {

    public final static TMobileTextToJsonNullReferenceResolver TEXT_TO_NULL_JSON_RESOLVER = new TMobileTextToJsonNullReferenceResolver();

    @Override
    public Optional<Object> returnObjectWhenCanBeResolved(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey) {
        if (propertyValue == null || propertyValue.equals(NULL_VALUE)) {
            return Optional.of(NULL_OBJECT);
        }
        return Optional.empty();
    }
}
