package com.tmobile.propertiestojson.resolvers;

import com.google.common.collect.ImmutableList;
import com.tmobile.propertiestojson.TMobileJsonObjectFieldsValidator;
import com.tmobile.propertiestojson.object.*;
import com.tmobile.propertiestojson.path.TMobilePathMetadata;
import com.tmobile.propertiestojson.resolvers.hierarchy.TMobileJsonTypeResolversHierarchyResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileNullToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileObjectToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToConcreteObjectResolver;
import com.tmobile.propertiestojson.util.exception.TMobileCannotOverrideFieldException;

import java.util.List;
import java.util.Optional;

import static com.tmobile.propertiestojson.TMobileJsonObjectFieldsValidator.isArrayJson;
import static com.tmobile.propertiestojson.object.TMobileJsonNullReferenceType.NULL_OBJECT;


public class TMobilePrimitiveJsonTypesResolver extends TMobileJsonTypeResolver {

    private final List<TMobileTextToConcreteObjectResolver> toObjectsResolvers;
    private final TMobileJsonTypeResolversHierarchyResolver resolversHierarchyResolver;
    private final Boolean skipNulls;
    private final TMobileNullToJsonTypeConverter nullToJsonTypeConverter;

    public TMobilePrimitiveJsonTypesResolver(List<TMobileTextToConcreteObjectResolver> toObjectsResolvers,
                                      List<TMobileObjectToJsonTypeConverter> toJsonResolvers,
                                      Boolean skipNulls,
                                      TMobileNullToJsonTypeConverter nullToJsonTypeConverter) {
        this.toObjectsResolvers = ImmutableList.copyOf(toObjectsResolvers);
        this.resolversHierarchyResolver = new TMobileJsonTypeResolversHierarchyResolver(toJsonResolvers);
        this.skipNulls = skipNulls;
        this.nullToJsonTypeConverter = nullToJsonTypeConverter;
    }

    @Override
    public TMobileObjectJsonType traverse(TMobilePathMetadata currentPathMetaData) {
        addPrimitiveFieldWhenIsValid(currentPathMetaData);
        return null;
    }

    private void addPrimitiveFieldWhenIsValid(TMobilePathMetadata currentPathMetaData) {
        TMobileJsonObjectFieldsValidator.checkThatFieldCanBeSet(currentObjectJsonType, currentPathMetaData, propertyKey);
        addPrimitiveFieldToCurrentJsonObject(currentPathMetaData);
    }

    private void addPrimitiveFieldToCurrentJsonObject(TMobilePathMetadata currentPathMetaData) {
        String field = currentPathMetaData.getFieldName();
        if(currentPathMetaData.isArrayField()) {
            addFieldToArray(currentPathMetaData);
        } else {
            if(currentObjectJsonType.containsField(field) && isArrayJson(currentObjectJsonType.getField(field))) {
                TMobileAbstractJsonType abstractJsonType = currentPathMetaData.getJsonValue();
                TMobileArrayJsonType currentArrayInObject = currentObjectJsonType.getJsonArray(field);
                if(isArrayJson(abstractJsonType)) {
                    TMobileArrayJsonType newArray = (TMobileArrayJsonType) abstractJsonType;
                    List<TMobileAbstractJsonType> abstractJsonTypes = newArray.convertToListWithoutRealNull();
                    for(int i = 0; i < abstractJsonTypes.size(); i++) {
                        currentArrayInObject.addElement(i, abstractJsonTypes.get(i), currentPathMetaData);
                    }
                } else {
                    throw new TMobileCannotOverrideFieldException(currentPathMetaData.getCurrentFullPath(), currentArrayInObject, propertyKey);
                }
            } else {
                currentObjectJsonType.addField(field, currentPathMetaData.getJsonValue(), currentPathMetaData);
            }
        }
    }

    public Object getResolvedObject(String propertyValue, String propertyKey) {
        Optional<?> objectOptional = Optional.empty();
        for(TMobileTextToConcreteObjectResolver primitiveResolver : toObjectsResolvers) {
            if(!objectOptional.isPresent()) {
                objectOptional = primitiveResolver.returnConvertedValueForClearedText(this, propertyValue, propertyKey);
            }
        }
        return objectOptional.orElse(null);
    }

    public TMobileAbstractJsonType resolvePrimitiveTypeAndReturn(Object propertyValue, String propertyKey) {
        TMobileAbstractJsonType result;
        if(propertyValue == null) {
            result = nullToJsonTypeConverter.convertToJsonTypeOrEmpty(this, NULL_OBJECT, propertyKey).get();
        } else {
            result = resolversHierarchyResolver.returnConcreteJsonTypeObject(this, propertyValue, propertyKey);
        }

        if(skipNulls && result instanceof TMobileJsonNullReferenceType) {
            result = TMobileSkipJsonField.SKIP_JSON_FIELD;
        }

        return result;
    }

    protected void addFieldToArray(TMobilePathMetadata currentPathMetaData) {
        if(arrayWithGivenFieldNameExist(currentPathMetaData.getFieldName())) {
            fetchArrayAndAddElement(currentPathMetaData);
        } else {
            createArrayAndAddElement(currentPathMetaData);
        }
    }

    private boolean arrayWithGivenFieldNameExist(String field) {
        return currentObjectJsonType.containsField(field);
    }

    private void createArrayAndAddElement(TMobilePathMetadata currentPathMetaData) {
        TMobileArrayJsonType arrayJsonTypeObject = new TMobileArrayJsonType();
        addElementToArray(currentPathMetaData, arrayJsonTypeObject);
        currentObjectJsonType.addField(currentPathMetaData.getFieldName(), arrayJsonTypeObject, currentPathMetaData);
    }

    private void fetchArrayAndAddElement(TMobilePathMetadata currentPathMetaData) {
        TMobileArrayJsonType arrayJsonType = getArrayJsonWhenIsValid(currentPathMetaData);
        addElementToArray(currentPathMetaData, arrayJsonType);
    }

    private void addElementToArray(TMobilePathMetadata currentPathMetaData, TMobileArrayJsonType arrayJsonTypeObject) {
        arrayJsonTypeObject.addElement(currentPathMetaData.getPropertyArrayHelper(), currentPathMetaData.getJsonValue(), currentPathMetaData);
    }

}
