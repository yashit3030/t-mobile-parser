package com.tmobile.propertiestojson.resolvers;



import com.tmobile.propertiestojson.TMobileJsonObjectFieldsValidator;
import com.tmobile.propertiestojson.TMobilePropertyArrayHelper;
import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileArrayJsonType;

import com.tmobile.propertiestojson.object.TMobileObjectJsonType;
import com.tmobile.propertiestojson.path.TMobilePathMetadata;

import java.util.List;

import static com.tmobile.propertiestojson.object.TMobileArrayJsonType.createOrGetNextDimensionOfArray;
import static com.tmobile.propertiestojson.util.TMobileListUtil.isLastIndex;


public class TMobileArrayJsonTypeResolver extends TMobileJsonTypeResolver {

    @Override
    public TMobileObjectJsonType traverse(TMobilePathMetadata currentPathMetaData) {
        fetchJsonObjectAndCreateArrayWhenNotExist(currentPathMetaData);
        return currentObjectJsonType;
    }

    private void fetchJsonObjectAndCreateArrayWhenNotExist(TMobilePathMetadata currentPathMetaData) {
        if(isArrayExist(currentPathMetaData.getFieldName())) {
            fetchArrayAndAddElement(currentPathMetaData);
        } else {
            createArrayAndAddElement(currentPathMetaData);
        }
    }

    private boolean isArrayExist(String field) {
        return currentObjectJsonType.containsField(field);
    }

    private void fetchArrayAndAddElement(TMobilePathMetadata currentPathMetaData) {
        TMobilePropertyArrayHelper propertyArrayHelper = currentPathMetaData.getPropertyArrayHelper();
        TMobileArrayJsonType arrayJsonType = getArrayJsonWhenIsValid(currentPathMetaData);
        List<Integer> dimIndexes = propertyArrayHelper.getDimensionalIndexes();
        TMobileArrayJsonType currentArray = arrayJsonType;
        for(int index = 0; index < dimIndexes.size(); index++) {
            if(isLastIndex(dimIndexes, index)) {
                int lastDimIndex = dimIndexes.get(index);
                if(currentArray.existElementByGivenIndex(lastDimIndex)) {
                    fetchJsonObjectWhenIsValid(currentPathMetaData, lastDimIndex, currentArray);
                } else {
                    createJsonObjectAndAddToArray(lastDimIndex, currentArray, currentPathMetaData);
                }
            } else {
                currentArray = createOrGetNextDimensionOfArray(currentArray, dimIndexes, index, currentPathMetaData);
            }
        }
    }

    private void createJsonObjectAndAddToArray(int index, TMobileArrayJsonType arrayJsonType, TMobilePathMetadata currentPathMetaData) {
        TMobileObjectJsonType nextObjectJsonType = new TMobileObjectJsonType();
        arrayJsonType.addElement(index, nextObjectJsonType, currentPathMetaData);
        currentObjectJsonType = nextObjectJsonType;
    }

    private void fetchJsonObjectWhenIsValid(TMobilePathMetadata currentPathMetaData, int index, TMobileArrayJsonType arrayJsonType) {
        TMobileAbstractJsonType element = arrayJsonType.getElement(index);
        TMobileJsonObjectFieldsValidator.checkEarlierWasJsonObject(currentPathMetaData.getOriginalPropertyKey(), currentPathMetaData, element);
        currentObjectJsonType = (TMobileObjectJsonType) element;
    }

    private void createArrayAndAddElement(TMobilePathMetadata currentPathMetaData) {
        TMobileArrayJsonType arrayJsonTypeObject = new TMobileArrayJsonType();
        TMobileObjectJsonType nextObjectJsonType = new TMobileObjectJsonType();
        arrayJsonTypeObject.addElement(currentPathMetaData.getPropertyArrayHelper(), nextObjectJsonType, currentPathMetaData);
        currentObjectJsonType.addField(currentPathMetaData.getFieldName(), arrayJsonTypeObject, currentPathMetaData);
        currentObjectJsonType = nextObjectJsonType;
    }
}
