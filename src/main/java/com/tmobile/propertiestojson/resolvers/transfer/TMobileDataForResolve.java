package com.tmobile.propertiestojson.resolvers.transfer;


import com.tmobile.propertiestojson.object.TMobileObjectJsonType;
import com.tmobile.propertiestojson.path.TMobilePathMetadata;

import java.util.Map;

public class TMobileDataForResolve {

    private final Map<String, String> properties;
    private final String propertyKey;
    private final TMobileObjectJsonType currentObjectJsonType;
    private final TMobilePathMetadata currentPathMetaData;

    public TMobileDataForResolve(Map<String, String> properties, String propertyKey, TMobileObjectJsonType currentObjectJsonType, TMobilePathMetadata currentPathMetaData) {
        this.properties = properties;
        this.propertyKey = propertyKey;
        this.currentObjectJsonType = currentObjectJsonType;
        this.currentPathMetaData = currentPathMetaData;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public String getPropertiesKey() {
        return propertyKey;
    }

    public TMobileObjectJsonType getCurrentObjectJsonType() {
        return currentObjectJsonType;
    }

    public TMobilePathMetadata getCurrentPathMetaData() {
        return currentPathMetaData;
    }
}
