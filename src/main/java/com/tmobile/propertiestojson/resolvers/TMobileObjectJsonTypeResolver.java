package com.tmobile.propertiestojson.resolvers;

import com.tmobile.propertiestojson.TMobileJsonObjectFieldsValidator;
import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileObjectJsonType;
import com.tmobile.propertiestojson.path.TMobilePathMetadata;

public class TMobileObjectJsonTypeResolver extends TMobileJsonTypeResolver {


    @Override
    public TMobileObjectJsonType traverse(TMobilePathMetadata currentPathMetaData) {
        fetchJsonObjectOrCreate(currentPathMetaData);
        return currentObjectJsonType;
    }

    private void fetchJsonObjectOrCreate(TMobilePathMetadata currentPathMetaData) {
        if (currentObjectJsonType.containsField(currentPathMetaData.getFieldName())) {
            fetchJsonObjectWhenIsNotPrimitive(currentPathMetaData);
        } else {
            createNewJsonObjectAndAssignToCurrent(currentPathMetaData);
        }
    }

    private void createNewJsonObjectAndAssignToCurrent(TMobilePathMetadata currentPathMetaData) {
        TMobileObjectJsonType nextObjectJsonType = new TMobileObjectJsonType();
        currentObjectJsonType.addField(currentPathMetaData.getFieldName(), nextObjectJsonType, currentPathMetaData);
        currentObjectJsonType = nextObjectJsonType;
    }

    private void fetchJsonObjectWhenIsNotPrimitive(TMobilePathMetadata currentPathMetaData) {
        TMobileAbstractJsonType jsonType = currentObjectJsonType.getField(currentPathMetaData.getFieldName());
        TMobileJsonObjectFieldsValidator.checkEarlierWasJsonObject(propertyKey, currentPathMetaData, jsonType);
        currentObjectJsonType = (TMobileObjectJsonType) jsonType;
    }
}
