package com.tmobile.propertiestojson.object;


import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileObjectToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToConcreteObjectResolver;

/**
 * This is object for notify that given reference will be converted as null in json.
 * It can be returned by
 * and can be returned by
 */
public final class TMobileJsonNullReferenceType extends TMobileAbstractJsonType {

    public final static TMobileJsonNullReferenceType NULL_OBJECT = new TMobileJsonNullReferenceType();

    public final static String NULL_VALUE = "null";

    @Override
    public String toStringJson() {
        return NULL_VALUE;
    }
}
