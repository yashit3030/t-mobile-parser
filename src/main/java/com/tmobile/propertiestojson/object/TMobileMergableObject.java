package com.tmobile.propertiestojson.object;


import com.tmobile.propertiestojson.path.TMobilePathMetadata;
import com.tmobile.propertiestojson.util.exception.TMobileMergeObjectException;

@SuppressWarnings("unchecked")
public interface TMobileMergableObject<T extends TMobileAbstractJsonType> {
    void merge(T mergeWith, TMobilePathMetadata currentPathMetadata);

    static void mergeObjectIfPossible(TMobileAbstractJsonType oldJsonElement, TMobileAbstractJsonType elementToAdd, TMobilePathMetadata currentPathMetadata) {
        TMobileMergableObject oldObject = (TMobileMergableObject) oldJsonElement;
        if (oldObject.getClass().isAssignableFrom(elementToAdd.getClass())) {
            oldObject.merge(elementToAdd, currentPathMetadata);
        } else {
            throw new TMobileMergeObjectException(oldJsonElement, elementToAdd, currentPathMetadata);
        }
    }
}
