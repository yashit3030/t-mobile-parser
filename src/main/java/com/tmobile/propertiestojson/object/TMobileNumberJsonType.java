package com.tmobile.propertiestojson.object;


public class TMobileNumberJsonType extends TMobilePrimitiveJsonType<Number> {

    public TMobileNumberJsonType(Number value) {
        super(value);
    }
}
