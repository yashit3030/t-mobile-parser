package com.tmobile.propertiestojson.object;



import com.tmobile.propertiestojson.path.TMobilePathMetadata;
import com.tmobile.propertiestojson.util.TMobileStringToJsonStringWrapper;
import com.tmobile.propertiestojson.util.exception.TMobileCannotOverrideFieldException;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.tmobile.parser.utils.TMobileConstants.*;
import static com.tmobile.propertiestojson.util.TMobileListUtil.getLastIndex;


public class TMobileObjectJsonType extends TMobileAbstractJsonType implements TMobileMergableObject<TMobileObjectJsonType> {

    private Map<String, TMobileAbstractJsonType> fields = new LinkedHashMap<>();

    public void addField(final String field, final TMobileAbstractJsonType object, TMobilePathMetadata currentPathMetaData) {
        if(object instanceof TMobileSkipJsonField) {
            return;
        }

        TMobileAbstractJsonType oldFieldValue = fields.get(field);
        if(oldFieldValue != null) {
            if(oldFieldValue instanceof TMobileMergableObject && object instanceof TMobileMergableObject) {
                TMobileMergableObject.mergeObjectIfPossible(oldFieldValue, object, currentPathMetaData);
            } else {
                throw new TMobileCannotOverrideFieldException(currentPathMetaData.getCurrentFullPath(),
                                                       oldFieldValue,
                                                       currentPathMetaData.getOriginalPropertyKey());
            }
        } else {
            fields.put(field, object);
        }
    }

    public boolean containsField(String field) {
        return fields.containsKey(field);
    }

    public TMobileAbstractJsonType getField(String field) {
        return fields.get(field);
    }

    public TMobileArrayJsonType getJsonArray(String field) {
        return (TMobileArrayJsonType) fields.get(field);
    }

    @Override
    public String toStringJson() {
        StringBuilder result = new StringBuilder().append(JSON_OBJECT_START);
        int index = 0;
        int lastIndex = getLastIndex(fields.keySet());
        for(String fieldName : fields.keySet()) {
            TMobileAbstractJsonType object = fields.get(fieldName);
            String lastSign = index == lastIndex ? EMPTY_STRING : NEW_LINE_SIGN;
            result.append(TMobileStringToJsonStringWrapper.wrap(fieldName))
                  .append(":")
                  .append(object.toStringJson())
                  .append(lastSign);
            index++;
        }
        result.append(JSON_OBJECT_END);
        return result.toString();
    }

    @Override
    public void merge(TMobileObjectJsonType mergeWith, TMobilePathMetadata currentPathMetadata) {
        for(String fieldName : mergeWith.fields.keySet()) {
            addField(fieldName, mergeWith.getField(fieldName), currentPathMetadata);
        }
    }
}
