package com.tmobile.propertiestojson.object;



public class TMobileBooleanJsonType extends TMobilePrimitiveJsonType<Boolean> {

    public TMobileBooleanJsonType(Boolean value) {
        super(value);
    }
}
