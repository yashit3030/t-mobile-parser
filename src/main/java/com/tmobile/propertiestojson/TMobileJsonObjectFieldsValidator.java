package com.tmobile.propertiestojson;


import com.tmobile.propertiestojson.object.*;
import com.tmobile.propertiestojson.path.TMobilePathMetadata;
import com.tmobile.propertiestojson.util.exception.TMobileCannotOverrideFieldException;

public class TMobileJsonObjectFieldsValidator {

    public static void checkThatFieldCanBeSet(TMobileObjectJsonType currentObjectJson, TMobilePathMetadata currentPathMetaData, String propertyKey) {
        if(currentObjectJson.containsField(currentPathMetaData.getFieldName())) {
            TMobileAbstractJsonType abstractJsonType = currentObjectJson.getField(currentPathMetaData.getFieldName());
            if(currentPathMetaData.isArrayField()) {
                if(isArrayJson(abstractJsonType)) {
                    TMobileArrayJsonType jsonArray = currentObjectJson.getJsonArray(currentPathMetaData.getFieldName());
                    TMobileAbstractJsonType elementByDimArray = jsonArray.getElementByGivenDimIndexes(currentPathMetaData);
                    if(elementByDimArray != null) {
                        throwErrorWhenCannotMerge(currentPathMetaData, propertyKey, elementByDimArray);
                    }
                } else {
                    throw new TMobileCannotOverrideFieldException(currentPathMetaData.getCurrentFullPathWithoutIndexes(), abstractJsonType, propertyKey);
                }
            } else {
                throwErrorWhenCannotMerge(currentPathMetaData, propertyKey, abstractJsonType);
            }
        }
    }

    private static void throwErrorWhenCannotMerge(TMobilePathMetadata currentPathMetaData, String propertyKey, TMobileAbstractJsonType oldJsonValue) {
        if (!isMergableJsonType(oldJsonValue) ) {
            throw new TMobileCannotOverrideFieldException(currentPathMetaData.getCurrentFullPath(), oldJsonValue, propertyKey);
        }
    }

    public static void checkEarlierWasJsonObject(String propertyKey, TMobilePathMetadata currentPathMetaData, TMobileAbstractJsonType jsonType) {
         if (!isObjectJson(jsonType)) {
             throw new TMobileCannotOverrideFieldException(currentPathMetaData.getCurrentFullPath(), jsonType, propertyKey);
         }
    }

    public static boolean isObjectJson(TMobileAbstractJsonType jsonType) {
        return TMobileObjectJsonType.class.isAssignableFrom(jsonType.getClass());
    }

    public static boolean isPrimitiveValue(TMobileAbstractJsonType jsonType) {
        return TMobilePrimitiveJsonType.class.isAssignableFrom(jsonType.getClass()) || TMobileJsonNullReferenceType.class.isAssignableFrom(jsonType.getClass());
    }

    public static boolean isArrayJson(TMobileAbstractJsonType jsonType) {
        return TMobileArrayJsonType.class.isAssignableFrom(jsonType.getClass());
    }

    public static boolean isMergableJsonType(Object jsonType) {
        return TMobileMergableObject.class.isAssignableFrom(jsonType.getClass());
    }
}
