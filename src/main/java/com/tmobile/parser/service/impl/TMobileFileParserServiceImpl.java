package com.tmobile.parser.service.impl;

import com.tmobile.parser.exception.TMobileFileParserException;
import com.tmobile.parser.model.HAProxyConfigModel;
import com.tmobile.parser.service.TMobileFileParserService;
import com.tmobile.parser.utils.TMobileParserUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.tmobile.parser.utils.TMobileConstants.*;

@Service
public class TMobileFileParserServiceImpl implements TMobileFileParserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TMobileFileParserServiceImpl.class);
    @Autowired
    TMobileParserUtils utils;
    @Override
    public List<HAProxyConfigModel> parseConfigFileToProperties(MultipartFile multipartFile) throws Exception {
        String fileName = null;
        List<HAProxyConfigModel> newList = new LinkedList<>();
        try {
            if (!multipartFile.isEmpty()) {
                byte[] bytes = multipartFile.getBytes();
                fileName = multipartFile.getOriginalFilename();
                File directory = new File(COMMON_FILE_LOCATION);
                if (!directory.exists()) {
                    LOGGER.info("Directory Not found... Creating a new Directory...");
                    directory.mkdir();
                }
                // Write the multipart contents to a file
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(COMMON_FILE_LOCATION + File.separator + fileName)));
                stream.write(bytes);
                stream.flush();
                stream.close();
                newList = utils.parseConfig(COMMON_FILE_LOCATION + File.separator + fileName);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            return newList;
        }
    }

    @Override
    public String convertPropertiesToYaml(List<String> props) throws Exception {
        String yamlContent = EMPTY_STRING;
        if (props != null && !props.isEmpty()) {
            long startTime = System.currentTimeMillis();
            List<HAProxyConfigModel> modelList = utils.constructObject(props);
            long endTime = (System.currentTimeMillis() - startTime);
            //System.out.println("++++++++++++++++ Time taken to constructObject :" + endTime + " : MS.");
            startTime = System.currentTimeMillis();
            modelList = utils.constructKeys(modelList);
            endTime = (System.currentTimeMillis() - startTime)/1000;
            //System.out.println("++++++++++++++++ Time taken to construct Keys :" + endTime + " : Secs.");
            String propFilePattern = new SimpleDateFormat(PROP_FILE_SAVE_PATTERN).format(new Date());
            String propFile = COMMON_FILE_LOCATION + File.separator + propFilePattern;
            // Write To Properties file
            Map<String, String> propMap = utils.writeToPropertiesFile(propFile, modelList);
            //--------------------- JSON Convertor
            String jsonFilePattern = new SimpleDateFormat(JSON_FILE_SAVE_PATTERN).format(new Date());
            String jsonFile = COMMON_FILE_LOCATION + File.separator + jsonFilePattern;
            String jsonContent = utils.writeToJsonFile(propFile, jsonFile);
            //String jsonContent = TMobileParserUtils.writeToJsonFileSecond(propMap, jsonFile);
            //--------------------- Yaml Convertor
            String yamlFilePattern = new SimpleDateFormat(YAML_FILE_SAVE_PATTERN).format(new Date());
            String yamlFile = COMMON_FILE_LOCATION + File.separator + yamlFilePattern;
            yamlContent = utils.writeToYamlFile(jsonContent, yamlFile);
            System.out.println("------------- All Process Over, Files written in folder :" + COMMON_FILE_LOCATION);
        } else {
            throw new TMobileFileParserException("No Proerties Found to create Yaml File");
        }
        return yamlContent;
    }
}
