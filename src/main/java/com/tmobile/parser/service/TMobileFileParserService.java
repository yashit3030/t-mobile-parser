package com.tmobile.parser.service;

import com.tmobile.parser.exception.TMobileFileParserException;
import com.tmobile.parser.model.HAProxyConfigModel;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface TMobileFileParserService {
    List<HAProxyConfigModel> parseConfigFileToProperties(MultipartFile multipartFile) throws Exception;

    String convertPropertiesToYaml(List<String> props) throws Exception;
}
