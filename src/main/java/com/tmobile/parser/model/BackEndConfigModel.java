package com.tmobile.parser.model;

import com.tmobile.standalone.HAProxyConfigModel;

import java.util.List;

public class BackEndConfigModel {
    List<HAProxyConfigModel> backendConfig;
    int index;
    public List<HAProxyConfigModel> getBackendConfig() {
        return backendConfig;
    }

    public void setBackendConfig(List<HAProxyConfigModel> backendConfig) {
        this.backendConfig = backendConfig;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
