package com.tmobile.parser.exception;

public class TMobileFileParserException extends Exception {
    public TMobileFileParserException(String exception) {
        // Call constructor of parent Exception
        super(exception);
    }
}