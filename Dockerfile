FROM openjdk:8
RUN apt-get update
COPY /target/tmobile-parser-1.0.war tmobile-utility.war
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "tmobile-utility.war"]
Env name T-Mobile-Parser
